var fs = require("fs");
var events = require("events");
var emitter = new events.EventEmitter();

var readFile = function(filename, callback) {
    fs.readFile(filename, 'utf8', callback);
};

var processFileRecord = function(record) {
    console.log(record);
};

var handleError = function(error) {
    console.log("An error occurred: ");
    console.log(error);
};

readFile(process.argv[2], function(error, data) {
    if (error) {
        emitter.emit("error", error);
    } else {
        var records = data.split("\n");
        for (var i = 0; i < records.length; i++) {
            emitter.emit("file_record_available", records[i]);
        }
    } 
});

emitter.on("error", handleError);
emitter.on("file_record_available", processFileRecord);
