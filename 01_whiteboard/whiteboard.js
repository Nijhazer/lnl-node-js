var io = require('socket.io').listen(10080);

var drawCache = [];

io.sockets.on('connection', function (socket) {
  socket.emit('drawingsReceived', drawCache);
  socket.on('draw', function(data) {
    drawCache.push(data);
    socket.emit('drawingReceived', data);
    socket.broadcast.emit('drawingReceived', data);
  });
  socket.on('sendClear', function() {
    drawCache = [];
    socket.emit('clear');
    socket.broadcast.emit('clear');
  });
});
