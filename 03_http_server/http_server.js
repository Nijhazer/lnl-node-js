var http = require('http');
var parse = require('url').parse;
var join = require('path').join;
var fs = require('fs');

var root = __dirname;

var http = require('http');
var server = http.createServer(function(request, response) {
    var url = parse(request.url);
    var path = join(root, url.pathname);
    var stream = fs.createReadStream(path);
    stream.pipe(response);
});
server.listen(10080);

server.on("error", function(error) {
    console.log("Hey, this error occurred, man:");
    console.log(error);
});
