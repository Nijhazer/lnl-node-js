var connect = require('connect');
var parse = require('url').parse;
var join = require('path').join;
var fs = require('fs');

var root = __dirname;

var logAccessRequest = function(request, response, next) {
    console.log("%s %s", request.method, request.url);
    next();
};

var processArguments = function(request, response, next) {
    if (request.query) {
        console.log("Query arguments:");
        console.log(request.query);    
    }    
};

var verifyAdminCredentials = function(request, response, next) {
    var authToken = request.headers.authorization;
    if (!authToken) {
        next(new Error("You're not supposed to be here"));
    }
    next();
};

var processAdminRequest = function(request, response, next) {
    response.end("You're approved! Have fun.");
};

var end = function(request, response) {
    response.end();
};

connect()
    .use(logAccessRequest)
    .use(connect.compress())
    .use(connect.static("content"))
    .use(connect.query())
    .use(processArguments)
    .use('/private', verifyAdminCredentials)
    .use('/private', processAdminRequest)
    .use(end)
    .listen(10080);
