var events = require("events");
var emitter = new events.EventEmitter();

var mongoose = require('mongoose');
mongoose.connect("mongodb://visualization-testing:visualization-testing@ds061518.mongolab.com:61518/visualization-testing");

var db = mongoose.connection;
db.on("open", function() {
    var locationSchema = mongoose.Schema({
        city: {
            countryCode: String,
            countryName: String,
            region: String,
            city: String,
            postalCode: String,
            latitude: Number,
            longitude: Number,
            dma_code: Number,
            area_code: Number,
            metro_code: Number
        },
        country: {
            code: String,
            name: String
        },
        ipAddress: String
    });
    var Location = mongoose.model("Location", locationSchema);

    Location.find().where("country.name").equals("United States").exec(function(err, results) {
        console.log(results);
        emitter.emit("close_db");
    });
});

db.on("error", function(error) {
    console.log("DB error:");
    console.log(error);
});

emitter.on("close_db", function() {
    db.close();
});
